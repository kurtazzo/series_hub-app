package com.example.series_hub;

import android.content.ClipData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FavoritesActivity extends AppCompatActivity implements Adapter.OnItemClickLister {

    public static final String url = "url";
    public static final String seriesName = "name";
    public static final String seriesPara = "para";

    private RecyclerView eRecyclerView;
    private FavoritesAdapter eAdapter;
    private ArrayList<FavoritesGetter> eCardList;
    private RequestQueue eRequestQueue;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    DatabaseReference db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView nav = (NavigationView)findViewById(R.id.nav);
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if (id == R.id.home){
                    startActivity(new Intent(FavoritesActivity.this, MainActivity.class));
                }
                if (id == R.id.search){
                    startActivity(new Intent(FavoritesActivity.this, SearchFieldActivity.class));
                }
                if (id == R.id.favorites){
                    startActivity(new Intent(FavoritesActivity.this, FavoritesActivity.class));
                }
                if (id == R.id.quit){
                    startActivity(new Intent(FavoritesActivity.this, PinActivity.class));
                    moveTaskToBack(true);
                }
                return true;
            }
        });

        eRecyclerView = findViewById(R.id.recycler_view);
        eRecyclerView.setHasFixedSize(true);
        eRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        eRequestQueue = Volley.newRequestQueue(this);

        db = FirebaseDatabase.getInstance().getReference("series");
        eCardList = new ArrayList<>();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        db.addValueEventListener(new ValueEventListener() {



            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                eCardList.clear();

                for (DataSnapshot seriesSnapshot : dataSnapshot.getChildren()){
                    FavoritesGetter series = seriesSnapshot.getValue(FavoritesGetter.class);

                    eCardList.add(series);
                }

                eAdapter = new FavoritesAdapter(FavoritesActivity.this, eCardList);
                eRecyclerView.setAdapter(eAdapter);
                eAdapter.SetOnItemClickListener(new FavoritesAdapter.OnItemClickLister() {
                    @Override
                    public void onItemClick(int position) {
                        Intent details = new Intent(getApplicationContext(), CardDetailActivity.class);
                        FavoritesGetter clicked = eCardList.get(position);

                        details.putExtra(url, clicked.getSeriesImg());
                        details.putExtra(seriesName, clicked.getSeriesName());
                        details.putExtra(seriesPara, clicked.getSeriesPara());

                        startActivity(details);
                    }

                });
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("loadPost:onCancelled", databaseError.toException());
            }
        });

    }


    @Override
    public void onItemClick(int position) {
    }
}
