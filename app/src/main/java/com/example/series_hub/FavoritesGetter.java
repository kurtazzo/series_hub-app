package com.example.series_hub;

public class FavoritesGetter {

    private String seriesId;
    private String seriesName;
    private String seriesPara;
    private String seriesImg;

    public FavoritesGetter(){

    }
    public FavoritesGetter(String seriesId, String seriesName, String seriesPara, String seriesImg){
        this.setSeriesId(seriesId);
        this.setSeriesName(seriesName);
        this.setSeriesPara(seriesPara);
        this.setSeriesImg(seriesImg);
    }

    public String getSeriesId() {
        return seriesId;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public String getSeriesPara() {
        return seriesPara;
    }

    public String getSeriesImg() {
        return seriesImg;
    }


    public void setSeriesId(String seriesId) {
        this.seriesId = seriesId;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public void setSeriesPara(String seriesPara) {
        this.seriesPara = seriesPara;
    }

    public void setSeriesImg(String seriesImg) {
        this.seriesImg = seriesImg;
    }
}
