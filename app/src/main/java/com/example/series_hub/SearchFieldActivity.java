package com.example.series_hub;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SearchFieldActivity extends AppCompatActivity {

    EditText searchField;
    SharedPreferences prefs;
    String Text;

    Date today;
    SimpleDateFormat format;
    String DateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchfield);

        searchField = findViewById(R.id.search_field);
        prefs = PreferenceManager.getDefaultSharedPreferences(SearchFieldActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Text = prefs.getString("text","");
        searchField.setText(Text);

        setContentView(R.layout.activity_searchfield);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, DateTime, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Text = prefs.getString("text","");
        searchField.setText(Text);

        setContentView(R.layout.activity_searchfield);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, DateTime, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Text = searchField.getText().toString();

        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("text", Text);
        edit.apply();

        today = new Date();
        format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        DateTime = format.format(today);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Text = searchField.getText().toString();

        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("text", Text);
        edit.apply();

        today = new Date();
        format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        DateTime = format.format(today);

    }


    public void searchClicked(View view){

        String searchAns = searchField.getText().toString();

        Intent sendData = new Intent(this,SearchActivity.class);
        sendData.putExtra("Data",searchAns);
        startActivity(sendData);
    }
}
