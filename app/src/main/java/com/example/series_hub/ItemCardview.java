package com.example.series_hub;

public class ItemCardview {
    private String eImageUrl;
    private String eSeriesTitle;
    private  String eSeriesPara;

    public ItemCardview(String imageUrl, String seriesTitle, String seriesPara){
        eImageUrl = imageUrl;
        eSeriesTitle = seriesTitle;
        eSeriesPara = seriesPara;
    }

    public String getImageUrl(){
        return eImageUrl;
    }

    public String getSeriesTitle(){
        return eSeriesTitle;
    }

    public String geteSeriesPara(){
        return eSeriesPara;
    }
}
