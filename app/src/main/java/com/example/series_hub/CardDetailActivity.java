package com.example.series_hub;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import static com.example.series_hub.MainActivity.seriesName;
import static com.example.series_hub.MainActivity.url;
import static com.example.series_hub.MainActivity.seriesPara;

public class CardDetailActivity extends AppCompatActivity {

    DatabaseReference db;

    ImageView imageView;
    TextView textViewCreator;
    TextView textViewPara;

    String img;
    String name;
    String para;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carddetail);

        Intent intent = getIntent();
        img = intent.getStringExtra(url);
        name = intent.getStringExtra(seriesName);
        para = intent.getStringExtra(seriesPara);

        imageView = findViewById(R.id.detailview);
        textViewCreator = findViewById(R.id.detailviewtext);
        textViewPara = findViewById(R.id.detailviewpara);

        Picasso.with(this).load("http://image.tmdb.org/t/p/w500/" + img).fit().centerInside().into(imageView);
        textViewCreator.setText(name);
        textViewPara.setText(para);

        registerForContextMenu(imageView);

        db = FirebaseDatabase.getInstance().getReference("series");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.dropdownmenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opt1:
                String id = db.push().getKey();

                FavoritesGetter favorite = new FavoritesGetter(id, name, para, img);

                db.child(id).setValue(favorite);

                Toast.makeText(this, "Added to Favorites", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void Youtube (View view){
        Intent intent = getIntent();
        String name = intent.getStringExtra(seriesName);
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/results?search_query=" + name)));
    }
}
