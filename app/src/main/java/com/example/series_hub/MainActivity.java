package com.example.series_hub;

import android.content.ClipData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Adapter.OnItemClickLister {

    public static final String url = "url";
    public static final String seriesName = "name";
    public static final String seriesPara = "para";

    private RecyclerView eRecyclerView;
    private Adapter eAdapter;
    private ArrayList<ItemCardview> eCardList;
    private RequestQueue eRequestQueue;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView nav = (NavigationView)findViewById(R.id.nav);
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if (id == R.id.home){
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                }
                if (id == R.id.search){
                    startActivity(new Intent(MainActivity.this, SearchFieldActivity.class));
                }
                if (id == R.id.favorites){
                    startActivity(new Intent(MainActivity.this, FavoritesActivity.class));
                }
                if (id == R.id.quit){
                    startActivity(new Intent(MainActivity.this, PinActivity.class));
                    moveTaskToBack(true);
                }
                return true;
            }
        });

        eRecyclerView = findViewById(R.id.recycler_view);
        eRecyclerView.setHasFixedSize(true);
        eRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        eRequestQueue = Volley.newRequestQueue(this);
        parseJSON();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void parseJSON(){
        String url = "http://api.themoviedb.org/3/tv/popular?api_key=1b5adf76a72a13bad99b8fc0c68cb085";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("results");

                    eCardList = new ArrayList<>();

                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject results = jsonArray.getJSONObject(i);

                        String seriesName = results.getString("original_name");
                        String imageUrl = results.getString("poster_path");
                        String seriesPara = results.getString("overview");

                        eCardList.add(new ItemCardview(imageUrl, seriesName, seriesPara));
                    }

                    eAdapter = new Adapter(MainActivity.this, eCardList);
                    eRecyclerView.setAdapter(eAdapter);
                    eAdapter.SetOnItemClickListener(MainActivity.this);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        eRequestQueue.add(request);
    }


    @Override
    public void onItemClick(int position) {
        Intent details = new Intent(this, CardDetailActivity.class);
        ItemCardview clicked = eCardList.get(position);

        details.putExtra(url, clicked.getImageUrl());
        details.putExtra(seriesName, clicked.getSeriesTitle());
        details.putExtra(seriesPara, clicked.geteSeriesPara());

        startActivity(details);
    }
}
