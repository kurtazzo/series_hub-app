package com.example.series_hub;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> {

    private Context eContext;
    private ArrayList<FavoritesGetter> eList;
    private OnItemClickLister eListener;

    public interface OnItemClickLister{
        void onItemClick(int position);
    }

    public void SetOnItemClickListener(OnItemClickLister listener){
        eListener = listener;
    }

    public FavoritesAdapter (Context context, ArrayList<FavoritesGetter> list){
        eContext = context;
        eList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(eContext).inflate(R.layout.cardview_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        FavoritesGetter currentItem = eList.get(i);

        String imageUrl = currentItem.getSeriesImg();
        String seriesName = currentItem.getSeriesName();
        String seriesPara = currentItem.getSeriesPara();

        viewHolder.eTextView.setText(seriesName);
        viewHolder.eTextViewPara.setText(seriesPara);
        Picasso.with(eContext).load("http://image.tmdb.org/t/p/w500/" + imageUrl).fit().centerInside().into(viewHolder.eImageView);
    }

    @Override
    public int getItemCount() {
        return eList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView eImageView;
        public TextView eTextView;
        public TextView eTextViewPara;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            eImageView = itemView.findViewById(R.id.image_view);
            eTextView = itemView.findViewById(R.id.text_view);
            eTextViewPara = itemView.findViewById(R.id.text_view_para);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(eListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            eListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }



}
