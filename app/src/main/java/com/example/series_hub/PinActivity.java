package com.example.series_hub;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.ShowType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class PinActivity extends AppCompatActivity {

    private BlurLockView view;
    private ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        view = (BlurLockView)findViewById(R.id.blurLookView);
        view.setBlurredView(image);

        image = (ImageView)findViewById(R.id.backgroundImageView);

        view.setCorrectPassword("0000");
        view.setLeftButton("");
        view.setRightButton("");
        view.setTypeface(Typeface.DEFAULT);
        view.setType(Password.NUMBER,false);

        view.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                startActivity(new Intent(PinActivity.this, MainActivity.class));
            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(PinActivity.this, "password Incorrect", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {
            }
        });
    }
}
